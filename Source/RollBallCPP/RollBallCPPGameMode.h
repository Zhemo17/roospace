// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RollBallCPPGameMode.generated.h"

UCLASS(minimalapi)
class ARollBallCPPGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ARollBallCPPGameMode();
};



