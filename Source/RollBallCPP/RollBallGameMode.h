// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RollBallGameMode.generated.h"

/**
 * 
 */
UCLASS()
class ROLLBALLCPP_API ARollBallGameMode : public AGameModeBase
{
	GENERATED_BODY()

	public:
	ARollBallGameMode();
	
};
