// Copyright Epic Games, Inc. All Rights Reserved.

#include "RollBallCPPGameMode.h"
#include "RollBallCPPBall.h"

ARollBallCPPGameMode::ARollBallCPPGameMode()
{
	// set default pawn class to our ball
	DefaultPawnClass = ARollBallCPPBall::StaticClass();
}
